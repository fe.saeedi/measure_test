﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DomainsTest
{
    [TestClass]
    public class FormulaEngineTest
    {
        private readonly IFormulaEngine _engine;
        public FormulaEngineTest()
        {
            this._engine = new FormulaEngine();
        }

        [TestMethod]
        [DataRow("( ( a + 3) * 8 )", true, "valid exp")]
        [DataRow("( ( a + 3) * 8 + 2 )", true, "valid exp with longer terms")]
        [DataRow("( ( a + 3) * 8 ))", false, "not valid exp when prantez count is not equal")]
        [DataRow("( ( a + 3) 8 ))", false, "not valid exp when oprators is not")]
        [DataRow("( ( a + 3) + 8 + b )", false, "not valid exp when two variabe exist")]
        [DataRow("( ( a + 3 ) & 8 )", false, "not valid exp when oprators not valid")]

        public void IsValid_Success(string inputExp, bool expected)
        {
            var actualResult = this._engine.IsValid(inputExp);

            Assert.AreEqual(expected, actualResult, $"parse {inputExp} not equal with expected result");
        }

        [TestMethod]
        [DataRow("( ( a + 3) * 8 )", 2, 40)]
        public void Process_Success(string inputExp, float inputAmount, float expected)
        {
            var actualResult = this._engine.Process(inputExp, inputAmount);

            Assert.AreEqual(expected, actualResult, $"parse result {inputExp} not equal with expected result");
        }

        [TestMethod]
        [DataRow("( ( a + 3) * 8 ))", 2, "error in exp")]
        [DataRow("( ( a + 3 + 5 ) * 8 )", 2, "error in tree")]
        [DataRow("( ( a + 3 ) & 8 )", 2, "error in exp")]
        public void Process_Failure(string inputExp, float inputAmount, string expctedErrorMessage)
        {
            try
            {

                var actualResult = this._engine.Process(inputExp, inputAmount);

                Assert.Fail($"parse result {inputExp} not equal with expected result");
            }
            catch (Exception exp)
            {
                Assert.AreEqual(expctedErrorMessage, exp.Message, ($"parse result {inputExp} not equal with expected result"));

            }
        }
    }
}
