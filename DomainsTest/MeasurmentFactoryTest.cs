﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainsTest
{
    [TestClass]
    public class MeasurmentFactoryTest
    {
        #region Fields
        private readonly IMeasurmentFactory _factory;
        private readonly Mock<IQueryMeasurmentRepository> _repo;
        private readonly Mock<IFormulaEngine> _engine;
        #endregion

        #region CTO
        public MeasurmentFactoryTest()
        {
            this._repo = new Mock<IQueryMeasurmentRepository>();
            this._engine = new Mock<IFormulaEngine>();
            this._factory = new MeasurmentFactory(this._repo.Object, this._engine.Object);
        }
        #endregion

        #region Methods
        [TestMethod]
        public void GetBaseInstance_SymbolFailure()
        {
            var inputSymbol = "%";
            var expectedErroMsg = "not valid symbol";
            try
            {
                this._factory.GetInstance(inputSymbol);

                Assert.Fail();
            }
            catch (Exception exp)
            {
                Assert.AreEqual(expectedErroMsg, exp.Message);
            }
        }
        [TestMethod]
        public void GetBaseInstance_DublicateSymbolFailure()
        {
            var inputSymbol = "kg";
            var expectedErroMsg = "olny one base object is valid";

            this._repo.Setup(r => r.GetBaseMeasureBySymblo(It.IsAny<string>())).ReturnsAsync(new Measurement(inputSymbol));
            try
            {
                this._factory.GetInstance(inputSymbol);

                Assert.Fail();
            }
            catch (Exception exp)
            {
                Assert.AreEqual(expectedErroMsg, exp.Message);
            }
        }
        [TestMethod]
        public void GetBaseInstance_Success()
        {
            var inputSymbol = "m";
            var expectedSymbol = "m";
            var expectedType = typeof(Measurement);


            this._repo.Setup(r => r.GetBaseMeasureBySymblo(It.IsAny<string>())).ReturnsAsync((Measurement)null);
            var actualResult = this._factory.GetInstance(inputSymbol).Result;

            Assert.AreEqual(expectedType, actualResult.GetType());
            Assert.AreEqual(expectedSymbol, actualResult.Dimension.Symbol);
        }
        [TestMethod]
        [DataRow("#","", "", "0")]
        [DataRow("m","&",  "", "0")]
        [DataRow("m","cm", "( 1 + a )", "0.01")]
        public void GetInstance_InputsFailure(string baseInputSymbol, string inputSymbol, string formula, float coffi)
        {
            var expectedErroMsg = "not valid params";
            try
            {
                this._factory.GetInstance(baseInputSymbol, inputSymbol, formula, coffi);

                Assert.Fail();

            }
            catch (Exception exp)
            {
                Assert.AreEqual(expectedErroMsg, exp.Message);
            }
        }
        [TestMethod]
        [DataRow("m","cm", "", "0.01")]
        public void GetInstance_Base_Failure(string baseInputSymbol, string inputSymbol, string formula, float coffi)
        {
            var expectedErroMsg = "base object is not exist";
            try
            {

                this._repo.Setup(r => r.GetBaseMeasureBySymblo(It.IsAny<string>())).ReturnsAsync((Measurement)null);
                this._factory.GetInstance(baseInputSymbol, inputSymbol, formula, coffi);

                Assert.Fail();
            }
            catch (Exception exp)
            {
                Assert.AreEqual(expectedErroMsg, exp.Message);
            }
        }
        [TestMethod]
        [DataRow("c", "f", "( c - 32  / 8", "0")]
        public void GetInstance_Formula_Failure(string baseInputSymbol, string inputSymbol, string formula, float coffi)
        {
            var expectedErroMsg = "furmula is not valid";
            try
            {

                this._repo.Setup(r => r.GetBaseMeasureBySymblo(It.IsAny<string>())).ReturnsAsync(new Measurement(baseInputSymbol));
                this._engine.Setup(r => r.IsValid(It.IsAny<string>())).Returns(false);
                this._factory.GetInstance(baseInputSymbol, inputSymbol, formula, coffi);

                Assert.Fail();
            }
            catch (Exception exp)
            {
                Assert.AreEqual(expectedErroMsg, exp.Message);
            }
        }
        [TestMethod]
        [DataRow("c", "f", "( ( f - 32 ) / 1.8 )", "0")]
        public void GetInstance_Formula_Success(string baseInputSymbol, string inputSymbol, string formula, float coffi)
        {
            var expectedSymbol = inputSymbol;
            var expectedFormula = formula;
            var expectedType = typeof(FormulateMeasurment);
            var expectedBaseSymbol = baseInputSymbol;


            this._repo.Setup(r => r.GetBaseMeasureBySymblo(It.IsAny<string>())).ReturnsAsync(new Measurement(baseInputSymbol));
            this._engine.Setup(r => r.IsValid(It.IsAny<string>())).Returns(true);
            var actualResult = this._factory.GetInstance(baseInputSymbol, inputSymbol, formula, coffi).Result;

            Assert.AreEqual(expectedType, actualResult.GetType());
            Assert.AreEqual(expectedSymbol, actualResult.Dimension.Symbol);
            Assert.AreEqual(expectedSymbol, ((FormulateMeasurment)actualResult).Formule);
            Assert.AreEqual(expectedBaseSymbol, ((FormulateMeasurment)actualResult).BaseSymble);
        }
        [TestMethod]
        [DataRow("m", "cm", "", "0.01")]
        public void GetInstance_Coefficient_Success(string baseInputSymbol, string inputSymbol, string formula, float coffi)
        {
            var expectedSymbol = inputSymbol;
            var expectedFormula = formula;
            var expectedType = typeof(CoefficientMeasurment);
            var expectedBaseSymbol = baseInputSymbol;

            this._repo.Setup(r => r.GetBaseMeasureBySymblo(It.IsAny<string>())).ReturnsAsync(new Measurement(baseInputSymbol));
            var actualResult = this._factory.GetInstance(baseInputSymbol, inputSymbol, formula, coffi).Result;

            Assert.AreEqual(expectedType, actualResult.GetType());
            Assert.AreEqual(expectedSymbol, actualResult.Dimension.Symbol);
            Assert.AreEqual(expectedSymbol, ((CoefficientMeasurment)actualResult).Coefficient);
            Assert.AreEqual(expectedBaseSymbol, ((CoefficientMeasurment)actualResult).BaseSymble);
        }
        #endregion
    }
}

