namespace Domain
{
    public class FormulateMeasurment : Measurement
    {
        #region CTO
        private FormulateMeasurment():base()
        {

        }
        public FormulateMeasurment(string baseSymble, string symble, string exp):base(symble)
        {
            this.Formule = exp;
            this.BaseSymble = baseSymble;
        }

        #endregion

        #region Props
        public string BaseSymble { get; set; }

        public string Formule { get; set; }
        #endregion

        public override float GetBaiseAmount(float amount, FormulaEngine engineProcessor)
        {
            return engineProcessor.Process(this.Formule, amount);
        }
    }
}