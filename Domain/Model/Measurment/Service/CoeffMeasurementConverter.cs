namespace Domain
{
    public class CoeffMeasurementConverter : IMeasurementConverter
    {
        private readonly FormulaEngine _engine;

        public CoeffMeasurementConverter(FormulaEngine engine)
        {
            this._engine = engine;

        }
        public float Parse(FormulateMeasurment source, CoefficientMeasurment destination, float amount)
        {
            var basicAmount = source.GetBaiseAmount(amount, this._engine);

            return destination.Coefficient * basicAmount;
        }
        public float Parse(CoefficientMeasurment source, CoefficientMeasurment destination, float amount)
        {
            var basicAmount = source.GetBaiseAmount(amount, this._engine);

            return destination.Coefficient * basicAmount;

        }
        public float Parse(Measurement source, CoefficientMeasurment destination, float amount)
        {
            var basicAmount = amount;

            return destination.Coefficient * basicAmount;
        }
        #region not imp
        public float Parse(FormulateMeasurment source, FormulateMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(CoefficientMeasurment source, FormulateMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(Measurement source, FormulateMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(FormulateMeasurment source, Measurement destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(CoefficientMeasurment source, Measurement destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(Measurement source, Measurement destination, float amount)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}