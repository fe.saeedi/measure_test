namespace Domain
{
    public class MeasurementConverter : IMeasurementConverter
    {
        private readonly FormulaEngine _engine;

        public MeasurementConverter(FormulaEngine engine)
        {
            this._engine = engine;
        }
        public float Parse(FormulateMeasurment source, Measurement destination, float amount)
        {
            return source.GetBaiseAmount(amount, this._engine);
        }
        public float Parse(CoefficientMeasurment source, Measurement destination, float amount)
        {
            return  source.GetBaiseAmount(amount, this._engine);
        }
        public float Parse(Measurement source, Measurement destination, float amount)
        {
            return amount;
        }
        #region not imp

        public float Parse(FormulateMeasurment source, CoefficientMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(CoefficientMeasurment source, CoefficientMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(Measurement source, CoefficientMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(FormulateMeasurment source, FormulateMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(CoefficientMeasurment source, FormulateMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }

        public float Parse(Measurement source, FormulateMeasurment destination, float amount)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}