namespace Domain
{
    public interface IMeasurementConverter
    {
        float Parse(FormulateMeasurment source, FormulateMeasurment destination, float amount);
        float Parse(CoefficientMeasurment source, FormulateMeasurment destination, float amount);
        float Parse(Measurement source, FormulateMeasurment destination, float amount);
        float Parse(FormulateMeasurment source, CoefficientMeasurment destination, float amount);
        float Parse(CoefficientMeasurment source, CoefficientMeasurment destination, float amount);
        float Parse(Measurement source, CoefficientMeasurment destination, float amount);
        float Parse(FormulateMeasurment source, Measurement destination, float amount);
        float Parse(CoefficientMeasurment source, Measurement destination, float amount);
        float Parse(Measurement source, Measurement destination, float amount);
    }
}