namespace Domain
{
    public class Dimension
    {
        private Dimension()
        {

        }
        public Dimension(string symbol)
        {
            this.Symbol = symbol;
        }
        public string Symbol { get; set; }
        public string Title { get; set; }
        public string TitleFa { get; set; }

    }
}