namespace Domain
{
    public class Measurement
    {
        protected Measurement() { }
        public Measurement(string symbol)
        {
            //get from ORM
            this.ID = 10;
            this.Dimension = new Dimension(symbol);
        }

        public int ID { get; set; }
        public Dimension Dimension { get; set; }

        public virtual float GetBaiseAmount(float amount, FormulaEngine engineProcessor)
        {
            return amount;
        }

    }
}
