namespace Domain
{
    public interface IMeasurementConverterFactory
    {
        IMeasurementConverter GetInstance(Measurement source, Measurement destination);
    }
}