using System.Threading.Tasks;

namespace Domain
{
    public interface IMeasurmentFactory
    {
        Task<Measurement> GetInstance(string baseSymbol, string symbol, string formula, float coffi);
        Task<Measurement> GetInstance(string symbol);
    }
}