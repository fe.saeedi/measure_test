using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class MeasurmentFactory: IMeasurmentFactory
    {
        #region Fields
        private readonly IQueryMeasurmentRepository _queryMeasurRepo;
        private readonly IFormulaEngine _formulaProcessEngine;
        #endregion

        #region CTO
        public MeasurmentFactory(IQueryMeasurmentRepository queryMeasurRepo, IFormulaEngine formulaProcessEngine)
        {
            this._queryMeasurRepo = queryMeasurRepo;
            this._formulaProcessEngine = formulaProcessEngine;
        }
        #endregion

        #region Methods
        public async Task<Measurement> GetInstance(string symbol)
        {
            
            if (!symbol.Any(c => !char.IsLetter(c)))
            {
                throw new System.Exception("not valid symbol");
            }
            else
            {
                var dbMeasure = await _queryMeasurRepo.GetBaseMeasureBySymblo(symbol);
                if (dbMeasure != null)
                {
                    throw new System.Exception("olny one base object is valid");
                }

                return new Measurement(symbol);
            }
        }
        public async Task<Measurement> GetInstance(string baseSymbol, string symbol, string formula, float coffi)
        {
            if (!baseSymbol.Any(c => !char.IsLetter(c))|| !symbol.Any(c => !char.IsLetter(c)) || (!string.IsNullOrEmpty(formula) && coffi != 0) || (string.IsNullOrEmpty(formula) && coffi == 0))
            {
                throw new System.Exception("not valid params");
            }
            else
            {
                var baseMeasure = await _queryMeasurRepo.GetBaseMeasureBySymblo(symbol);
                if (baseMeasure == null)
                {
                    throw new System.Exception("base object is not exist");
                }

                if (!string.IsNullOrEmpty(formula) && coffi == 0)
                {
                    if (!_formulaProcessEngine.IsValid(formula))
                        throw new System.Exception("furmula is not valid");

                    return new FormulateMeasurment(baseSymbol, symbol, formula);
                }
                else 
                {
                    return new CoefficientMeasurment(baseSymbol, symbol, coffi);
                }
            }
        }
        #endregion
    }
}