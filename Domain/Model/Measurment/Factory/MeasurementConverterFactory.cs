namespace Domain
{
    public class MeasurementConverterFactory : IMeasurementConverterFactory
    {
        private readonly FormulaEngine _engine;

        public MeasurementConverterFactory(FormulaEngine engine)
        {
            this._engine = engine;

        }
        public IMeasurementConverter GetInstance(Measurement source, Measurement destination)
        {
            if (typeof(CoefficientMeasurment) == destination.GetType())
                return new CoeffMeasurementConverter(this._engine);

            if (typeof(FormalMeasurementConverter) == destination.GetType())
                return new FormalMeasurementConverter(this._engine);

            return new MeasurementConverter(this._engine);
        }
    }
}