using System.Threading.Tasks;

namespace Domain
{
    public interface IQueryMeasurmentRepository
    {
        Task<Measurement> GetBaseMeasureBySymblo(string symbol);
        Task<Measurement> GetByID(int id);
    }
}