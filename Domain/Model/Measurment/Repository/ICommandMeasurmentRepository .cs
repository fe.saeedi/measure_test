using System.Threading.Tasks;

namespace Domain
{
    public interface ICommandMeasurmentRepository
    {
        Task<Measurement> Insert(Measurement measurement);
    }
}