namespace Domain
{
    public class CoefficientMeasurment : Measurement
    {
        #region CTO
        private CoefficientMeasurment() : base()
        {

        }
        public CoefficientMeasurment(string baseSymble, string symble, float coff) : base(symble)
        {
            this.Coefficient = coff;
            this.BaseSymble = baseSymble;
        }
        #endregion

        #region Props
        public float Coefficient { get; set; }
        public string BaseSymble { get; set; }
        #endregion

        public override float GetBaiseAmount(float amount, FormulaEngine engineProcessor)
        {
            return this.Coefficient*amount;
        }
    }
}