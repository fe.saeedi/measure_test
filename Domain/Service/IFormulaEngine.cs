namespace Domain
{
    public interface IFormulaEngine
    {
        float Process(string exp, float amount);
        bool IsValid(string exp);
    }
}