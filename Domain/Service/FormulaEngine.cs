using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class FormulaEngine : IFormulaEngine
    {
        #region Fields
        private const string _openParantez = "(";
        private const string _closeParantez = ")";
        private readonly List<string> _validOprators;
        private string _variableLetter;
        private Tree treeFormul;
        #endregion

        #region CTO
        public FormulaEngine()
        {
            this._variableLetter = "";
            this._validOprators = new List<string>() { "+", "-", "*", "/" };
            this.treeFormul = new Tree();
        }
        #endregion

        #region Methods
        public bool IsValid(string exp)
        {
            try
            {
                splitElementWithValidation(exp);

                return true;
            }
            catch
            {
                return false;
            }
        }
        public float Process(string exp, float amount)
        {
            List<Element> elements = splitElementWithValidation(exp);
            elements = replaceVriable(elements, amount);
            this.treeFormul = generateTree(elements);
            return this.treeFormul.Process(amount);
        }
        #endregion

        #region Private Methods
        private List<Element> replaceVriable(List<Element> stack, float amount)
        {
            for (int i = 0; i < stack.Count; i++)
            {
                if (stack[i].Type == ElementEnum.Variable)
                {
                    stack[i].Type = ElementEnum.Operand;
                    stack[i].Value = amount.ToString();
                }
            }

            return stack;
        }
        public Tree generateTree(List<Element> _stack)
        {
            Tree t = new Tree();
            t.parent = t;
            Tree nowTree = t;
            for (int i = 1; i < _stack.Count; i++)
            {
                if (_stack[i].Type == ElementEnum.OpenPrantez)
                {
                    nowTree.left = new Tree() { parent = nowTree };
                }
                else if (_stack[i].Type == ElementEnum.Operand)
                {
                    if (nowTree.left == null)
                        nowTree.left = new Tree() { parent = nowTree, left = null, right = null, root = _stack[i].Value };
                    else if (nowTree.right == null)
                        nowTree.right = new Tree() { parent = nowTree, left = null, right = null, root = _stack[i].Value };
                    else// must be handel in extract
                        throw new System.Exception("error in tree");
                }
                else if (_stack[i].Type == ElementEnum.Oprator)
                {
                    nowTree.root = _stack[i].Value;
                }
                else if (_stack[i].Type == ElementEnum.ClosePrantez)
                {
                    nowTree = nowTree.parent;
                }
            }
            return nowTree;
        }
        private List<Element> splitElementWithValidation(string exp)
        {
            var expArr = exp.Split(' ');
            if (expArr[0] != _openParantez) throw new System.Exception("error in exp");

            List<Element> elements = new List<Element>();
            elements.Add(new Element() { Value = expArr[0], Type = ElementEnum.OpenPrantez });
            string beforChar = "";
            string c = "";
            for (int i = 1; i < expArr.Length; i++)
            {
                c = expArr[i];
                beforChar = expArr[i - 1];

                this.checkedRules(elements, c, beforChar);

                if (c == _openParantez) elements.Add(new Element() { Value = c, Type = ElementEnum.OpenPrantez });
                else if (c == _closeParantez) elements.Add(new Element() { Value = c, Type = ElementEnum.ClosePrantez });
                else if (_validOprators.Contains(c)) elements.Add(new Element() { Value = c, Type = ElementEnum.Oprator });
                else
                {
                    var oprand = this.getOprand(c);
                    var vb = this.getVariable(c);
                    if (oprand.HasValue) elements.Add(new Element() { Value = c, Type = ElementEnum.Operand });
                    if (!string.IsNullOrEmpty(vb)) elements.Add(new Element() { Value = c, Type = ElementEnum.Variable });
                    else throw new System.Exception("error in exp");
                }
            }

            return elements;
        }
        private bool checkedRules(List<Element> elements, string nowChar, string beforChar)
        {
            if (_validOprators.Contains(beforChar) && getOprand(nowChar) == null)
                throw new System.Exception("err in exp");
            if (beforChar == _closeParantez && getOprand(nowChar) != null)
                throw new System.Exception("err in exp");
            if (nowChar == _closeParantez && elements.Count(x => x.Value == _closeParantez) + 1 > elements.Count(x => x.Value == _openParantez))
                throw new System.Exception("err in exp");
            if ((beforChar == _closeParantez && nowChar == _openParantez) || (beforChar == _openParantez && nowChar == _closeParantez))
                throw new System.Exception("err in exp");

            return true;
        }
        private float? getOprand(string c)
        {
            try
            {
                return float.Parse(c);
            }
            catch
            {
                return null;
            }
        }
        private string getVariable(string c)
        {
            if (!string.IsNullOrEmpty(c)) return null;
            if (!string.IsNullOrEmpty(c) && _variableLetter != c) return null;
            this._variableLetter = c;

            return this._variableLetter;
        }
        #endregion
    }
    public class Tree
    {
        public string root { get; set; }
        public Tree left { get; set; }
        public Tree right { get; set; }
        public Tree parent { get; set; }

        public float Process(float amount)
        {
            if (this.left == null && this.right == null) return float.Parse(root);
            var leftAmount = left.Process(amount);
            var rightAmount = right.Process(amount);
            return doOprator(root, leftAmount, rightAmount);

        }
        public float doOprator(string oprand, float leftOprand, float rightOprand)
        {
            switch (oprand)
            {
                case "+": return leftOprand + rightOprand;
                case "-": return leftOprand - rightOprand;
                case "*": return leftOprand * rightOprand;
                case "/": return leftOprand / rightOprand;
                default: throw new System.Exception("error in exp");
            }
        }
    }
    public class Element
    {
        public string Value { get; set; }
        public ElementEnum Type { get; set; }

    }
    public enum ElementEnum
    {
        OpenPrantez = 1,
        ClosePrantez = 2,
        Operand = 3,
        Oprator = 4,
        Variable = 5
    }
}