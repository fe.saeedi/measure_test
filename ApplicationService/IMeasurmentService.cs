﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices
{
    public interface IMeasurmentService
    {
        Task InsertBaseMeasurmentAsync(string symbol);
        Task InsertBaseMeasurmentAsync(string baseSymbol, string symbol, string formula, float coffi);
        Task<float> ConvertMeasurment(float amount, int sourceMeasureID, int destMeasureID);
    }
}
