using Domain;
using System.Threading.Tasks;

namespace ApplicationServices
{
    public class MeasurmentService: IMeasurmentService
    {
        #region Fields
        private readonly ICommandMeasurmentRepository _commandMeasurRepo;
        private readonly IQueryMeasurmentRepository _queryMeasurRepo;
        private readonly IMeasurmentFactory _measurFactory;
        private readonly IMeasurementConverterFactory _measureConverterFactory;

        #endregion

        #region CTO
        public MeasurmentService(ICommandMeasurmentRepository commandMeasurRepo, IQueryMeasurmentRepository queryMeasurRepo,
                              IMeasurmentFactory measurFactory, IMeasurementConverterFactory measureConverterFactory)
        {
            this._commandMeasurRepo = commandMeasurRepo;
            this._queryMeasurRepo = queryMeasurRepo;
            this._measurFactory = measurFactory;
            this._measureConverterFactory = measureConverterFactory;
        }
        #endregion

        #region Methods
        public async Task InsertBaseMeasurmentAsync(string symbol)
        {
            var measur = await _measurFactory.GetInstance(symbol);
            await _commandMeasurRepo.Insert(measur);
        }
        public async Task InsertBaseMeasurmentAsync(string baseSymbol, string symbol, string formula, float coffi)
        {
            var measur = await _measurFactory.GetInstance(baseSymbol, symbol, formula, coffi);
            await _commandMeasurRepo.Insert(measur);
        }
        public async Task<float> ConvertMeasurment(float amount, int sourceMeasureID, int destMeasureID)
        {
            var sourceMeasure = await _queryMeasurRepo.GetByID(sourceMeasureID);
            var destMeasure = await _queryMeasurRepo.GetByID(destMeasureID);
            var converter = _measureConverterFactory.GetInstance(sourceMeasure, destMeasure);

            return converter.Parse(sourceMeasure, destMeasure, amount);
        }
        #endregion

    }
}